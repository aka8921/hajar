import React, { Component } from 'react';
import './App.css';
import { Attendance } from './components/Attendance';

class App extends Component {
  render() {
    return (
      <div className="App" >
        <Attendance />
      </div>
    );
  }
}



export default App;
