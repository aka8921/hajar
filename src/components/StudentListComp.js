import React, { Component } from 'react';


class StudentListComp extends Component {
  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = { status: 1 };
  }
  colorChnage(id){
    var bg = ["red", "white"];
    var fg = ["white","black"]
    //alert(id)
    var newstate = this.state.status;
    if(this.state.status === 0) {newstate = 1}
    else{newstate = 0}
    document.getElementById(id).style.color = fg[newstate];
    document.getElementById(id).style.backgroundColor = bg[newstate];
    this.setState({status: newstate })
    
  } 
  render() {
    return(
      <div  style = {divstyle} id = {this.props.identity } onClick={() => {this.colorChnage(this.props.identity)}}>{this.props.studentname}</div>
    )
  }
}


const divstyle = {
  padding:20,
  fontSize: '15px',
  textAlign: 'center'
};

export default StudentListComp;