import React, { Component } from 'react';
import PeriodList from './PeriodList';
import AttendanceTaking from './AttendanceTaking';
import Confirm from './Confirm';
import Success from './Success';

export class Attendance extends Component {
  state = {
    step: 1,
    classes: [
      "XI A", "XI B", "XI C","XII A", "XII B", "XII C",
    ]
  };

  // Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    if(this.state.step === 3){
      this.setState({
        step: 1
      });
    }
    else{
      this.setState({
        step: step + 1
      });
    }
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };

  // Handle fields change
  // handleChange = input => e => {
  //   this.setState({ [input]: e.target.value });
  // };

  render() {
    const { step } = this.state;
    //const classes = { classes };

    switch (step) {
      case 1:
        return (
          <PeriodList
            nextStep={this.nextStep}
            classes={this.state.classes}
          />
        );
      case 2:
        return (
          <AttendanceTaking
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            //handleChange={this.handleChange}
            //values={values}
          />
        );
      case 3:
        return (
          <Success
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            //values={values}
          />
        );
      case 4:
        return <Success 
        nextStep={this.nextStep}
        prevStep={this.prevStep}
        />;
    }
  }
}

export default Attendance;
