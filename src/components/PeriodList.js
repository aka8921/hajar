import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
//import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import SimpleCard from './SimpleCard';

export class PeriodList extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    const { classes } = this.props;
    return (
      <MuiThemeProvider>
        <React.Fragment>
          <AppBar title="Hajar" />
          
          
          {this.props.classes.map((cls, index) => (
          <SimpleCard 
          period = {cls}
          nextStep = {this.props.nextStep}
          />
          ))}

        </React.Fragment>
      </MuiThemeProvider>
    );
}
}

const styles = {
  button: {
    margin: 15
  }
};

export default PeriodList;
