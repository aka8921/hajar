import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import RaisedButton from 'material-ui/RaisedButton';
import Typography from '@material-ui/core/Typography';
import { white } from 'material-ui/styles/colors';

const styles = {
  root: {
    justifyContent: 'center'
},
  card: {
    minWidth: 275,
    margin: 20
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  button: {
      margin: 15,
 }
};

function SimpleCard(props) {
  const { classes } = props;
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.card}>
      <CardContent>
      <Typography component="h3" variant="h3" gutterBottom>
          {props.period}
        </Typography>
      </CardContent>
      <CardActions classes={{root: classes.root}}>
        <RaisedButton
            label="GO!"
            primary={true}
            style={styles.button}
            onClick={props.nextStep}
          />
      </CardActions>
    </Card>
  );
}

SimpleCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleCard);