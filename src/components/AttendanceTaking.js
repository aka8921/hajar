import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import StudentListComp from './StudentListComp'

export class AttendanceTaking extends Component {
  constructor(props) 
    { 
        super(props); 
        this.state = { 
          students:[
            ["s1","Student1",0],
            ["s2","Student2",0],
            ["s3","Student3",0],
            ["s4","Student4",0],
            ["s5","Student5",0],
            ["s6","Student6",0],
            ["s7","Student7",0],
            ["s8","Student8",0],
            ["s9","Student9",0],
            ["s10","Student10",0],
            ["s11","Student11",0],
            ["s12","Student10",0],
            ["s13","Student13",0],
            ["s14","Student14",0],
            ["s15","Student15",0],
            ["s16","Student16",0],
            ["s17","Student17",0],
            ["s18","Student18",0],
            ["s19","Student19",0],
            ["s20","Student20",0],
            ["s21","Student21",0],
            ["s22","Student22",0],
            ["s23","Student23",0],
            ["s24","Student24",0],
            ["s25","Student25",0],
            ["s26","Student26",0],
            ["s27","Student27",0],
            ["s28","Student28",0],
            ["s29","Student29",0],
            ["s30","Student30",0],
            ["s31","Student31",0],
            ["s32","Student32",0],
            ["s33","Student33",0],
            ["s34","Student34",0],
            ["s35","Student35",0],
            ["s36","Student36",0],
            ["s37","Student37",0],
            ["s38","Student38",0],
            ["s39","Student39",0],
            ["s40","Student40",0],
          ] 
        }; 
    } 
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (
      <MuiThemeProvider>
        <React.Fragment>
          <AppBar title="Enter Personal Details" />
          {this.state.students.map((cls, index) => (
          <StudentListComp 
          studentname = {cls[1]}
          identity = {cls[0]}
          />
          ))}
          <RaisedButton
            label="Continue"
            primary={true}
            style={styles.button}
            onClick={this.continue}
          />
          <RaisedButton
            label="Go Back"
            primary={true}
            style={styles.button}
            onClick={this.back}
          />
        </React.Fragment>
      </MuiThemeProvider>
    );
  }
}

const styles = {
  button: {
    margin: 15
  }
};

export default AttendanceTaking;
